const api_head = "https://api.themoviedb.org/3/";
const api_key = "?api_key=152829e22a4f6c00ac21c47eacd60feb";
const img_src_w500 = "https://image.tmdb.org/t/p/w500";
let searchQuery;
let lengths = new Array();
async function moviesInHome(cur_page) {
  cleanPage();
  loading("#main");
  window.document.title = "Home";
  const req = `${api_head}trending/movie/week${api_key}&page= ${cur_page}`;
  const respone = await fetch(req);
  const rs = await respone.json();
  const data = rs.results;

  for (let i = 0; i < data.length; i++) {
    const req_length = `${api_head}movie/${data[i].id}${api_key}`;
    console.log(req_length);
    const rsp = await fetch(req_length);
    const length_rsp = await rsp.json();
    lengths.push(length_rsp.runtime);
  }
  cleanPage();
  $("#trending").append(
    `<h2 class="mx-5 text-white my-5">Top Trending Movies</h2>
      `
  );

  displayMovies(rs, "moviesInHome");
}

async function evtSubmit(e) {
  e.preventDefault();

  searchQuery = $("form input").val();
  window.location.href = `?page=1&query=${searchQuery}`;
  searchMovies(1);
}
async function searchMovies(page) {
  cleanPage();
  loading("#main");
  const reqStr = `${api_head}search/multi${api_key}&query=${searchQuery}&page=${page}`;
  window.document.title = "Search result: " + searchQuery;
  const reponse = await fetch(reqStr);
  const rs = await reponse.json();
  const data = rs.results;  
  for (let i = 0; i < data.length; i++) {
    const req_length = `${api_head}movie/${data[i].id}${api_key}`;
    console.log(req_length);
    const rsp = await fetch(req_length);
    const length_rsp = await rsp.json();
    lengths.push(length_rsp.runtime);
  }
  cleanPage();
  displayMovies(rs, "searchMovies");
}

async function displayMovies(result, mode) {
  const data = result.results;
  for (let j = 0; j < data.length; j++) {
    let d = data[j];
    if (d.media_type == "person" && d.known_for_department == "Acting") {
      let filmOfHim = new Array();
      let know_for = d.known_for;
      for (let i = 0; i < know_for.length; i++) {
        if (know_for[i].media_type != "movie") continue;

        $("#main").append(
          `<div class="card " style="width: 18rem;padding: 0.2rem; margin: 0.5rem 0 0 0.5rem;">
                    <img src="${img_src_w500}${
            know_for[i].poster_path
          }" class="card-img-top">
                    <div class="card-body">
                      <h5 class="card-title">${know_for[i].original_title}</h5>
                      <p class="card-text">${know_for[i].overview.substring(
                        0,
                        80
                      )}...</p>
                      <p class="card-text">Thời lượng: ${lengths[i]} phút</p>
                      <p class="card-text">Đánh giá: ${
                        know_for[i].vote_average
                      }/10</p>
                      <a  onClick="displayMovie(${
                        know_for[i].id
                      })" class="btn btn-primary">Chi tiết</a>
                      </div>
                  </div>`
        );
      }
    } else if (d.media_type == "movie") {
      $("#main").append(
        `<div class="card mx-1 my-1" style="width: 18rem;padding: 0.2rem; ">
                <img src="${img_src_w500}${d.poster_path}" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">${d.title}</h5>
                  <p class="card-text">${d.overview.substring(0, 80)}...</p>
                  <p class="card-text">Thời lượng: ${lengths[j]} phút</p>
                  <p class="card-text">Đánh giá: ${d.vote_average}/10</p>
                  <a  href="?id=${d.id}&tag=movie" onClick="displayMovie(${
          d.id
        })" class="btn btn-primary">Chi tiết</a>
                  </div>
              </div>`
      );
    }
  }
  $(".pagination").show();
  pagination(result, mode);
}

function loading(where) {
  $(where)
    .append(`<div class="spinner-border mx-auto my-auto text-light" role="status">
    <span class="sr-only ">Loading...</span>
  </div>`);
}

function cleanPage() {
  $("#main").empty();
  $("#trending").empty();
  $("#trailer").empty();
  $("#trailer").hide();
  $("#acting").empty();
  $("#nofti").empty();
  $(".pagination").hide();
  $("#reviews").empty();
  $("#reviews").hide();
}

function pagination(data, mode) {
  $(".pagination li:not(:first-child):not(:last-child)").remove();

  let cur_page = data.page;
  let total_pages = data.total_pages;
  if (mode == "moviesInHome") {
    $(".pagination li:last-child")
      .before(`<li class="page-item active" aria-current="page">
    <a class="page-link" href="#">${cur_page}<span class="sr-only">(current)</span></a>
           </li>`);
    let t = cur_page - 1;
    while (t >= cur_page - 5 && t > 0) {
      $(".pagination li:first-child")
        .after(`<li class="page-item" aria-current="page">
      <a class="page-link" href="?page=${t}"  onclick="${mode}(${t})">${t}<span class="sr-only">(current)</span></a>
             </li>`);
      t--;
    }
    t = cur_page + 1;
    while (t <= cur_page + 5 && t <= total_pages) {
      $(".pagination li:last-child")
        .before(`<li class="page-item" aria-current="page">
      <a class="page-link" href="?page=${t}"  onclick="${mode}(${t})">${t}<span class="sr-only">(current)</span></a>
             </li>`);
      t++;
    }

    $(".pagination li:first-child,li:last-child").remove();
    if (cur_page > 1) {
      $(".pagination li:first-child").before(
        ` <li class="page-item ">
      <a class="page-link" href="?page=${cur_page -
        1}" tabindex="-1" aria-disabled="true" onclick="${mode}(${cur_page -
          1}, ${total_pages})">&laquo;</a>
  </li>`
      );
    } else {
      $(".pagination li:first-child").before(
        ` <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1" aria-disabled="true" >&laquo;</a>
  </li>`
      );
    }

    if (cur_page < total_pages) {
      $(".pagination li:last-child").after(
        ` <li class="page-item ">
      <a class="page-link" href="?page=${cur_pages +
        1}" tabindex="-1" aria-disabled="true" onclick="${mode}(${cur_page +
          1}, ${total_pages})">&raquo;</a>
  </li>`
      );
    } else {
      $(".pagination li:last-child").after(
        ` <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1" aria-disabled="true" >&raquo;</a>
  </li>`
      );
    }

    $(".pagination li:first-child").before(
      ` <li class="page-item ">
    <a class="page-link" href="?page=1&query=" tabindex="-1" aria-disabled="true" onclick="${mode}(${1}, ${total_pages})">First</a>
</li>`
    );

    $(".pagination li:last-child").after(
      ` <li class="page-item ">
    <a class="page-link" href="?page=${total_pages}" tabindex="-1" aria-disabled="true" onclick="${mode}(${total_pages}, ${total_pages})">Last</a>
</li>`
    );
  } else {
    //else search
    $(".pagination li:last-child")
      .before(`<li class="page-item active" aria-current="page">
  <a class="page-link" href="#">${cur_page}<span class="sr-only">(current)</span></a>
         </li>`);
    let t = cur_page - 1;
    while (t >= cur_page - 5 && t > 0) {
      $(".pagination li:first-child")
        .after(`<li class="page-item" aria-current="page">
    <a class="page-link" href="?page=${t}&query=${searchQuery}"  onclick="${mode}(${t})">${t}<span class="sr-only">(current)</span></a>
           </li>`);
      t--;
    }
    t = cur_page + 1;
    while (t <= cur_page + 5 && t <= total_pages) {
      $(".pagination li:last-child")
        .before(`<li class="page-item" aria-current="page">
    <a class="page-link" href="?page=${t}&query=${searchQuery}"  onclick="${mode}(${t})">${t}<span class="sr-only">(current)</span></a>
           </li>`);
      t++;
    }

    $(".pagination li:first-child,li:last-child").remove();
    if (cur_page > 1) {
      $(".pagination li:first-child").before(
        ` <li class="page-item ">
    <a class="page-link" href="?page=${cur_page -
      1}&query=${searchQuery}" tabindex="-1" aria-disabled="true" onclick="${mode}(${cur_page -
          1}, ${total_pages})">&laquo;</a>
</li>`
      );
    } else {
      $(".pagination li:first-child").before(
        ` <li class="page-item disabled">
    <a class="page-link" href="#" tabindex="-1" aria-disabled="true" >&laquo;</a>
</li>`
      );
    }

    if (cur_page < total_pages) {
      $(".pagination li:last-child").after(
        ` <li class="page-item ">
    <a class="page-link" href="?page=${cur_page +
      1}&query=${searchQuery}" tabindex="-1" aria-disabled="true" onclick="${mode}(${cur_page +
          1}, ${total_pages})">&raquo;</a>
</li>`
      );
    } else {
      $(".pagination li:last-child").after(
        ` <li class="page-item disabled">
    <a class="page-link" href="#" tabindex="-1" aria-disabled="true" >&raquo;</a>
</li>`
      );
    }

    $(".pagination li:first-child").before(
      ` <li class="page-item ">
  <a class="page-link" href="?page=1&query=${searchQuery}" tabindex="-1" aria-disabled="true" onclick="${mode}(${1}, ${total_pages})">First</a>
</li>`
    );

    $(".pagination li:last-child").after(
      ` <li class="page-item ">
  <a class="page-link" href="?page=${total_pages}&query=${searchQuery}" tabindex="-1" aria-disabled="true" onclick="${mode}(${total_pages}, ${total_pages})">Last</a>
</li>`
    );
  }
}

async function displayMovie(id) {
  cleanPage();
  loading("#main");
  const req = `${api_head}movie/${id}${api_key}&append_to_response=videos,reviews`;

  const respone = await fetch(req);
  const rs = await respone.json();
  const name = rs.title;
  window.document.title = name;
  const overview = rs.overview;
  const poster = rs.poster_path;
  let genres = new Array();
  for (let i = 0; i < rs.genres.length; i++) {
    genres.push(rs.genres[i].name);
  }
  let acting = new Array();
  const acting_req = `${api_head}movie/${id}/casts${api_key}`;
  const acting_respone = await fetch(acting_req);
  const acting_rs = await acting_respone.json();
  let director = "Chưa cập nhật";
  for (let i = 0; i < acting_rs.cast.length; i++) {
    if (acting_rs.crew[i].department == "Directing") {
      director = acting_rs.crew[i].name;
      ``;
      break;
    }
  }

  cleanPage();

  $("#main").append(
    `<div class="card mb-3 mx-5 ">
        <div class="row no-gutters">
          <div class="col-md-3">
            <img src="${img_src_w500}${poster}" class="card-img" alt="...">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h2 class="card-title">${name}</h2>
              <p class="card-text"><b>Thể loại</b>&emsp;&emsp;&emsp;&emsp;&ensp;${genres}</p>
              <p class="card-text"><b>Ngày phát hành</b>&ensp;${rs.release_date}</p>
              <p class="card-text"><b>Thời lượng</b>&emsp;&emsp;&emsp;${rs.runtime} phút</p>
              <p class="card-text"><b>Đạo diễn</b>&emsp;&emsp;&emsp;&emsp;${director}</p>
              <p class="card-text"><b>Đánh giá</b>&emsp;&emsp;&emsp;&emsp;${rs.vote_average}/10</p>
              <p class="card-text"><b>Tóm tắt</b>&emsp;&emsp;&emsp;&emsp;&ensp;${rs.overview}</p>
              <p class="card-text"><b>Review</b>&emsp;&emsp;&emsp;&emsp;&ensp;${rs.overview}</p>

            </div>
          </div>
        </div>
      </div>`
  );
  if (rs.videos.results.length > 0) {
    $("#trailer").show();

    console.log(rs.videos);
    $("#trailer").append(
      `<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/${rs.videos.results[0].key}"></iframe>`
    );
    $("#nofti").append(
      `<h2 class="mx-5 text-white my-5">Các diễn viên tham gia</h2>`
    );
  }

  $("#reviews").show();
  $("#reviews").append(
    `<div class="card-header h1">
    Reviews
  </div>
  <div class="card-body">
  </div>`
  );
  const reviews_result = rs.reviews.results;
  for (let i = 0; i < reviews_result.length; i++) {
    $("#reviews .card-body").append(
      `    <h5 class="card-title">${reviews_result[i].author}</h5>
      <p class="card-text">${reviews_result[i].content}</p>`
    );
  }

  for (let i = 0; i < acting_rs.cast.length; i++) {
    $("#acting").append(
      `<div class="card" href="?id=${acting_rs.cast[i].id}&tag=cast" onclick="fillActor(${acting_rs.cast[i].id})" style="width: 10rem;padding: 0.2rem; margin: 0.5rem 0.5rem 0 0.5rem">
            <img src="${img_src_w500}${acting_rs.cast[i].profile_path}" class="card-img-top">
            <div class="card-body">
              <a class="card-text" >${acting_rs.cast[i].name}</a>    
              </div>
          </div>`
    );
  }
}

async function fillActor(id) {
  cleanPage();
  loading("#main");
  const req = `${api_head}person/${id}${api_key}`;

  const response = await fetch(req);
  const rs = await response.json();
  const gender = rs.gender == 2 ? "Nam" : "Nữ";
  let movies = new Array();
  const req_movies = `${api_head}person/${id}/movie_credits${api_key}`;
  const response_movies = await fetch(req_movies);
  const rs2 = await response_movies.json();
  let list = rs2.cast;
  window.document.title = rs.name;
  const biography = rs.biography === "" ? "Chưa cập nhật" : rs.biography;
  cleanPage();

  $("#main").append(
    `<div class="card mb-3  mx-auto">
    <div class="row no-gutters">
      <div class="col-md-3">
        <img src="${img_src_w500}${rs.profile_path}" class="card-img" alt="...">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h2 class="card-title">${rs.name}</h2>
          <p class="card-text"><b>Ngày sinh</b>&emsp;&ensp;${rs.birthday}</p>
          <p class="card-text"><b>Giới tính</b>&emsp;&ensp;${gender}</p>
          <p class="card-text"><b>Tiểu sử</b>&emsp;&ensp;&ensp;${biography}</p>
        </div>
      </div>
    </div>
  </div>`
  );

  $("#nofti").append(
    `<h2 class="mx-5 text-white my-5">Các phim đã tham gia</h2>`
  );
  for (let i = 0; i < list.length; i++) {
    $("#acting").append(
      `<div class="card mx-2 my-2" onclick="displayMovie(${list[i].id})" style="width: 18rem;padding: 0.2rem;">
      <img src="${img_src_w500}${list[i].backdrop_path}" class="card-img-top">
      <div class="card-body">
        <a class="card-text" >${list[i].original_title}</a>    
        </div>
    </div>`
    );
  }
}

function getUrlVars() {
  var vars = [],
    hash;
  var hashes = window.location.href
    .slice(window.location.href.indexOf("?") + 1)
    .split("&");
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split("=");
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  console.log(vars);
  return vars;
}

async function load() {
  let vars = getUrlVars();
  let page_number = vars.page == undefined ? 1 : parseInt(vars.page);
  if (vars.query) {
    searchQuery = vars.query;
    searchMovies(page_number);
  } else if (vars.tag == "movie") {
    displayMovie(vars.id);
  } else if (vars.tag == "cast") {
    fillActor(vars.id);
  } else {
    moviesInHome(page_number);
  }
}
